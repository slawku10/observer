package actors.impl;

import actors.Observer;
import actors.Subject;
import actors.WeatherData;

import java.util.ArrayList;
import java.util.List;

public class WeatherDataImpl implements Subject, WeatherData{

    private List<Observer> observerList;
    private float temparature;
    private float humidity;
    private float pressure;

    public WeatherDataImpl() {
        this.observerList = new ArrayList<>();
    }

    public void subscribeObserver(Observer observer) {
        observerList.add(observer);
    }

    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    public void notifyObservers() {
        observerList.forEach(o ->o.update(temparature, humidity, pressure));
    }

    public void setWheatherData(float temparature,
                                float humidity,
                                float pressure){
        this.temparature = temparature;
        this.humidity = humidity;
        this.pressure = pressure;

        changeWheatherData();
    }

    private void changeWheatherData() {
        notifyObservers();
    }
}
