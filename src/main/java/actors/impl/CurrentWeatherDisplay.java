package actors.impl;

import actors.Observer;
import actors.Subject;

public class CurrentWeatherDisplay implements Observer {

    private float temperature;
    private float humidity;
    private float pressure;

    private Subject subject;

    public CurrentWeatherDisplay(Subject subject) {
        this.subject = subject;
        subject.subscribeObserver(this);
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;

        display();
    }

    private void display() {
        System.out.println("Current wheather:"
                + "\ntemperature: " + temperature
                + "\nhumidity: " + humidity
                + "\npressure: " + pressure);
    }
}
