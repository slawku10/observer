package actors;

public interface WeatherData {
    void setWheatherData(float temparature,
                         float humidity,
                         float pressure);
}
