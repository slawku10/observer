package actors;

public interface Subject {

    void subscribeObserver(Observer observer);
    void removeObserver(Observer observer);
    void notifyObservers();
}
