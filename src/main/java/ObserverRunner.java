import actors.Observer;
import actors.Subject;
import actors.WeatherData;
import actors.impl.CurrentWeatherDisplay;
import actors.impl.WeatherDataImpl;

public class ObserverRunner {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherDataImpl();
        Observer currentWeatherDisplay = new CurrentWeatherDisplay(((Subject) weatherData));
        weatherData.setWheatherData(10, 15, 1000);
    }
}
